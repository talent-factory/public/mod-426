package javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class HelloFX extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("JavaFX App");

        @SuppressWarnings("MismatchedReadAndWriteOfArray") final File[] selectedFile = new File[1];
        FileChooser fileChooser = new FileChooser();

        Button button = new Button("Select File");

        button.setOnAction(e ->
                selectedFile[0] = fileChooser.showOpenDialog(stage));

        VBox vBox = new VBox(button);
        Scene scene = new Scene(vBox, 960, 600);

        stage.setScene(scene);
        stage.show();
    }

}
