package opencv;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class HelloCV {

    public static void main(String[] args) {
        //Load native library
        System.loadLibrary("opencv_java451");
        //image container object
        Mat imageArray;
        //Read image from file system
        imageArray = Imgcodecs.imread("src/main/resources/HappyFish.jpg");
        //Get image with & height
        System.out.println(imageArray.rows());
        System.out.println(imageArray.cols());
    }

}
